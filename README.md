# RpiSafeCam
# Intro
Installation and configuration to use an RPI with a camera module as a safety webcam which can send email if the image move an other stuff.

# Boot

## Préparation de la carte SD
Premierement il faut néttoyer toutes les partitions de la carte mémoire. Si la carte est /dev/sdc, il faut:

```
sudo fdisk /dev/sdc
```
puis entré `d` et entrée succéssivement les numéros des partitions présents sur la carte.

Une fois nettoyé, il est faut copier minibian sur la carte :
```
sudo dd bs=4M if=/home/jordan/Téléchargements/2016-03-12-jessie-minibian.img of=/dev/sdc
```

Voici ce qu'il doit s'afficher: 
```
198+1 enregistrements lus
198+1 enregistrements écrits
832569344 bytes (833 MB, 794 MiB) copied, 84,1697 s, 9,9 MB/s
```

On peut vérifier les partitions avec:
`sudo fdisk -l`
Il doit y avoir 2 partitions:
```
Périphérique Amorçage Début     Fin Secteurs Taille Id Type
/dev/sdc1                 16  125055   125040  61,1M  b W95 FAT32
/dev/sdc2             125056 1626111  1501056   733M 83 Linux
```
Ensuite, il faut augmenter l'espace disque de la partition principale grace a parted. :warning: Il ne faut pas prendre toute la place et laisser au moins 2Go
pour les images de la webcam. Cela permettra de mettre en lecture seule uniquement le système.

Pour cela, il faut démonter les partitions:
```
sudo umount /dev/sdc1
sudo umount /dev/sdc2
```

et lancer parted:
```
sudo parted
```

puis il faut utiliser la commande resizepart:

```
select /dev/sdc
resizepart 2 16G
```

Ici, on étend la partition à 16G.

Il est ensuite nécéssaire d'étendre le système de fichier sur toute la table.
Pour cela, taper les commandes suivantes:
```
sudo e2fsck -f /dev/sdc2
sudo resize2fs /dev/sdc2
```


## Configuration préalable

On peut configurer directement le firmware pour qu'il charge le driver de la camera en ajoutant `start_x=1` dans le fichier config.txt
```
mkdir /mnt/rpi
sudo mount /dev/sdc1 /mnt/rpi
sudo /bin/sh -c 'start_x=1>>mnt/rpi/config.txt'
```

## SSH
Maintenant que tout est configuré, on peut faire booter le RPI et l'utiliser a distance avec ssh. Pour cela, il faut un cadre ethernet à la box et un écran avec le port HDMI
pour voir directement l'adresse ip à l'écran. Une fois le boot terminé et le RPI affiche l'adresse ip obtenu par le DHCP.

Sur un autre pc, on peut se connecter en ssh via :
```
ssh root@MON_ADRESSE_IP
raspberry
```

Comme on peut le voir la connexion SSH est en root par défaut. Pour changer cela il faut créer un utilisateur :
```
adduser jordan
```

Ensuite, on peut se connecter par SSH sur ce compte pour être sure que cela marche:
```
ssh jordan@MON_ADRESSE_IP
MONT_PASSWORD
```

Par défaut, minibian ne possède pas sudo. Par simplicité, il faut l'installer:
```
apt update && apt install sudo
```

De même, il ne possède pas nano :
```
apt install nano
```

Puis il faut configurer le fichier sudoers pour autoriser l'utilisateur:
```
nano /etc/sudoers.d/jordan
```
Et ajouter la ligne suivante : `jordan    ALL=(ALL:ALL) ALL`

Ensuite, on peut configurer SSH pour ne pas autoriser sur root:
```
sudo nano /etc/ssh/sshd_config
```

Remplacer la ligne `PermitRootLogin=yes` par `PermitRootLogin=no`.
`CTRL+X` pour quiter nano et faire un restart du serveur SSH:
```
sudo service ssh restart
```
 et enfin on peut changer le message de root pour sécusier le rpi:
 ```
 sudo passwd root
 ```
 

##Lecture seule

Suivre le tuto ici :
https://david.mercereau.info/raspberrypi-raspbian-en-lecture-seul-readonly-pour-preserver-la-carte-sd/

En résumé :
supprimer les packets inutiles
```
sudo apt-get remove --purge logrotate dbus dphys-swapfile  fake-hwclock isc-dhcp-client  dhcpcd5  isc-dhcp-common
```

Comme il n'y a plus de DHCP, il faut mettre l'adresse ip en statique: ```sudo nano /etc/network/interfaces```

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
        address 192.168.0.14
        netmask 255.255.255.0
        gateway 192.168.0.1
```

Puis il faut ajouter les serveurs DNS. Ici on prendra openDNS:
```
sudo cp /etc/resolv.conf /etc/resolv.conf.back
sudo nano /etc/resolv.conf
```
Et ajouter ces lignes :

```
nameserver 208.67.222.222
nameserver 208.67.220.220
```

Ensuite il faut configurer sur la box une adresse statique pour notre adresse mac. Pour cela taper la commande suivante:
```ip addr``

Cette commande remplace ifonfig. Trouver l'adresse mac et aller sur la box pour configurer l'adresse ip. Ici l'adresse est 
192.168.0.14.


Puis redemarrer les services réseaux : ```sudo service network restart```

Faire un ping sur google.com pour valider.


Ensuite on peut configurer correctement l'horloge:
```
sudo rm /etc/localtime
sudo ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
```

Puis installer busibox à la place de ryslog car celui-ci fonctionne en RAM:
```
sudo apt-get install busybox-syslogd; dpkg --purge rsyslog
```

Ensuite il faut faire du ménage au démarrage: 
```
sudo insserv -r bootlogs
```


On désactive le bash_history soit en supprimant complètement le fichier:
```
history -c
rm ~/.bash_history -rf
export HISTFILESIZE=0
unset HISTFILE
nano ~/.bashrc
```
Et ajouter cette ligne : ```HISTFILESIZE=0```


Pour faciliter le basculement entre read-only et read-write, deux commandes doivent être crées. Pur cela, il faut
modifier le fichier bash.bashrc:

```
sudo nano /etc/bash.bashrc
```

Puis ajouter ces lignes :

````
# Fonction pour connaître le mode en cours
fs_mode=$(mount | sed -n -e "s/^.* on \/ .*(\(r[w|o]\).*/\1/p")
# alias ro/rw pour passer de l'un à l'autre
alias ro='mount -o remount,ro /dev/mmcblk0p2 / ; fs_mode=$(mount | sed -n -e $
alias rw='mount -o remount,rw /dev/mmcblk0p2 / ; fs_mode=$(mount | sed -n -e $

#autre alias
alias sudo='sudo '
````


Ensuite, il faut modifier le fstab pour que les partitions soient automatiquement
monté en ro au demarrage:
````
sudo nano /etc/fstab
````

Puis remplacer les lignes existantes par :
`````
/dev/mmcblk0p1 /boot vfat defaults,ro 0 2
/dev/mmcblk0p2 / ext4 default,ro,noatime,nodiratime,commit=120 0 1
tmpfs /tmp tmpfs defaults,nodev,nosuid 0 0
tmpfs /var/log tmpfs defaults,nodev,nosuid 0 0
tmpfs /var/tmp tmpfs defaults,nodev,nosuid 0 0
`````


De même, remplacer le fichier /etc/cmdline.txt par cette ligne:
````
dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty$
````

Et voila, fini pour le read-only :)


#Installation de 




























